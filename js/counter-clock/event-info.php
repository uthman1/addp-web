<?php
// $dotenv->load();
require_once('../../vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createMutable($_SERVER["DOCUMENT_ROOT"]);
$dotenv->load();

$servername = $_ENV['SERVERNAME'];
$username = $_ENV['USERNAME'];
$password = $_ENV['PASSWORD'];
$dbname = $_ENV['DBNAME'];
$eventInfo = new stdClass();
header('Content-type: Application/json');

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT NAME, DATE, TIME FROM ADDF_EVENT_INFO WHERE NAME='Appreciation Dinner DF'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $eventInfo->name = $row["NAME"];
        $eventInfo->date = $row["DATE"];
        $eventInfo->time = $row["TIME"];
    }
    
    echo json_encode($eventInfo);
} else {
    echo "0 results";
}