export function counterFunc(element) {
    var countDownContainer = document.createElement('div');
    countDownContainer.className = "CDContainer"

    var poster = document.createElement('img');
    poster.className = "Poster"
    poster.src = "../../images/Poster1.jpg";

    var dayLabel = document.createElement('p');
    dayLabel.append("DAYS");
    dayLabel.className="CDText";

    var daysRemain = document.createElement('p');
    daysRemain.id = "daysRemain";
    daysRemain.className="CDText";
    daysRemain.append("00");

    var clock = document.createElement('p');
    clock.id = "timeRemain";
    clock.append("00 : 00 : 00");
    clock.style.cssText = "font-size:xx-large; font-weight: bold; padding: 0 0 1em 0;";

    var phpData = document.createElement('p');
    phpData.id = phpData;
    phpData.append("--");
    phpData.style.cssText = "font-size:xx-large; font-weight: bold; padding: 0 0 1em 0;";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../js/counter-clock/event-info.php");
    xhr.onload = function() {
        var data = this.response;
        console.log(data);
        startCountDown(JSON.parse(data));
    };
    xhr.send();
    countDownContainer.append(daysRemain);
    countDownContainer.append(dayLabel);
    element.append(poster);
    element.append(countDownContainer);
}

function startCountDown(event) {
    console.log(event);
    var eventDate = new Date(event.date + "T" + event.time);
    var remaining = { Days: "", Hours: "", Minutes: "", Seconds: "" };
    
    var nowDate = new Date();
    var countDown = eventDate.getTime() - nowDate.getTime();
    remaining.Days = Math.floor(countDown / (1000 * 60 * 60 * 24));
    remaining.Hours = Math.floor((countDown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    remaining.Minutes = Math.floor((countDown % (1000 * 60 * 60)) / (1000 * 60));
    remaining.Seconds = Math.floor((countDown % (1000 * 60)) / 1000);

    remaining.Hours = remaining.Hours < 10 ? "0" + remaining.Hours : remaining.Hours;
    remaining.Minutes = remaining.Minutes < 10 ? "0" + remaining.Minutes : remaining.Minutes;
    remaining.Seconds = remaining.Seconds < 10 ? "0" + remaining.Seconds : remaining.Seconds;

    var daysRemain = document.getElementById("daysRemain");
    var clock = document.getElementById("timeRemain");
    daysRemain.innerText = remaining.Days;
    // clock.innerText = "HH : MM : SS"
    // clock.innerText = clock.innerText.replace("HH", remaining.Hours);
    // clock.innerText = clock.innerText.replace("MM", remaining.Minutes);
    // clock.innerText = clock.innerText.replace("SS", remaining.Seconds);
}